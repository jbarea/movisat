<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>NombreTarea</th>
        <th>Descripcion</th>
        <th>Empleado Asignado</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tareas as $tarea)
      <tr>
        <td>{{$tarea['id']}}</td>
        <td>{{$tarea['NombreTarea']}}</td>
        <td>{{$tarea['Descripcion']}}</td>
        <td>{{$tarea['empleadoID']}}</td>
        <td><a href="{{action('TareasController@edit', $tarea['id'])}}" class="btn btn-warning">Editar</a></td>
        <td>
          <form action="{{action('TareasController@destroy', $tarea['id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Borrar</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>