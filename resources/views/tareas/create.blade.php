<!-- create.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Proyecto demo para Movisat </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Agregue una tarea a un empleado</h2><br  />
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
      <form method="post" action="{{url('tareas')}}">
      {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="NombreTarea">Nombre:</label>
            <input type="text" class="form-control" name="NombreTarea">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Descripcion">Descripcion:</label>
              <input type="text" class="form-control" name="Descripcion">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="empleadoID">Empleado:</label>
              <select type="text" class="form-control" name="empleadoID">
                @foreach($emple as $em)
                <option value="{{$em['id']}}">{{$em['nombre']}}</option>
                @endforeach 
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Añadir Tarea</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>