<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Tipo</th>
        <th>NºTareas</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($empleados as $empleado)
      <tr>
        <td>{{$empleado['id']}}</td>
        <td>{{$empleado['nombre']}}</td>
        <td>{{$empleado['apellido']}}</td>
        <td>{{$empleado['tipo']}}</td>
        <?php
            $result = App\Tarea::has('EmpleadoID','');
        ?>
        <td>{{$tarea['id']}}</td>
        <td><a href="{{action('EmpleadosController@edit', $empleado['id'])}}" class="btn btn-warning">Editar</a></td>
        <td>
          <form action="{{action('EmpleadosController@destroy', $empleado['id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Borrar</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>