<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $fillable = ['nombre','apellido','tipo'];

    public function tareas(){
        return $this->hasMany('App\Tarea');
    }
}
