<?php

namespace App\Http\Controllers;

use App\Tarea;
use App\Empleado;

use Illuminate\Http\Request;

class TareasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tareas = Tarea::all()->toArray();
        return view('tareas.index', compact('tareas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emple = Empleado::all()->toArray();
        return view('tareas.create', compact('emple'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tarea = $this->validate(request(), [
            'NombreTarea' => 'required',
            'Descripcion' => 'required',
            'empleadoID' => 'required'
        ]);

        Tarea::create($tarea);

        return back()->with('success', 'Tarea añadida');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tarea = Tarea::find($id);
        return view('tareas.edit',compact('tarea','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tarea = Tarea::find($id);
        $this->validate(request(), [
          'NombreTarea' => 'required',
          'Descripcion' => 'required',
          'empleadoID' => 'required'
        ]);
        $tarea->NombreTarea = $request->get('NombreTarea');
        $tarea->Descripcion = $request->get('Descripcion');
        $tarea->empleadoID = $request->get('empleadoID');
        $tarea->save();
        return redirect('tareas')->with('success','Tarea actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tarea = Tarea::find($id);
        $tarea->delete();
        return redirect('tareas')->with('success','Tarea eliminada');
    }
}
