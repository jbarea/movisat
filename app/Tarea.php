<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $fillable = ['NombreTarea','Descripcion','empleadoID'];

    public function empleados(){
        return $this->belongsTo('App\Empleado');
    } 
}
